%global maj_ver 15
%global min_ver 0
%global patch_ver 7

%global bin_suffix -%{maj_ver}
%global install_bindir %{_libdir}/llvm%{maj_ver}/bin

%global binary_list analyze-build bugpoint c-index-test clang clang++ clang-apply-replacements clang-change-namespace clang-check clang-cl clang-cpp clangd clang-doc clang-extdef-mapping clang-format clang-include-fixer clang-linker-wrapper clang-move clang-nvlink-wrapper clang-offload-bundler clang-offload-packager clang-offload-wrapper clang-pseudo clang-query clang-refactor clang-rename clang-reorder-fields clang-repl clang-scan-deps clang-tidy diagtool dsymutil find-all-symbols git-clang-format hmaptool intercept-build ld64.lld ld.lld llc lld lld-link lli llvm-addr2line llvm-ar llvm-as llvm-bcanalyzer llvm-bitcode-strip llvm-cat llvm-cfi-verify llvm-config llvm-cov llvm-c-test llvm-cvtres llvm-cxxdump llvm-cxxfilt llvm-cxxmap llvm-debuginfod llvm-debuginfod-find llvm-diff llvm-dis llvm-dlltool llvm-dwarfdump llvm-dwarfutil llvm-dwp llvm-exegesis llvm-extract llvm-gsymutil llvm-ifs llvm-install-name-tool llvm-jitlink llvm-lib llvm-libtool-darwin llvm-link llvm-lipo llvm-lto llvm-lto2 llvm-mc llvm-mca llvm-ml llvm-modextract llvm-mt llvm-nm llvm-objcopy llvm-objdump llvm-opt-report llvm-otool llvm-pdbutil llvm-profdata llvm-profgen llvm-ranlib llvm-rc llvm-readelf llvm-readobj llvm-reduce llvm-remark-size-diff llvm-rtdyld llvm-sim llvm-size llvm-split llvm-stress llvm-strings llvm-strip llvm-symbolizer llvm-tapi-diff llvm-tblgen llvm-tli-checker llvm-undname llvm-windres llvm-xray modularize opt pp-trace run-clang-tidy sancov sanstats scan-build scan-build-py scan-view split-file verify-uselistorder wasm-ld

Name:		clang
Version:	%{maj_ver}.%{min_ver}.%{patch_ver}
Release:	1
Summary:	A C language family front-end for LLVM
License:	NCSA
Requires:	clang%{maj_ver} = %{version}

%description
Clang project is a C, C++, Objective C and Objective C++ front-end
for the LLVM compiler. Its goal is to offer a replacement to the GNU Compiler
Collection (GCC).

Clang implements all of the ISO C++ 1998, 11 and 14 standards and also
provides most of the support of C++17.

This is a dependency package providing the default clang compiler.

%package -n clang-tools-extra
Summary:	Extra tools for clang
Requires:	clang%{maj_ver}-tools-extra = %{version}

%description -n clang-tools-extra
A set of extra tools built using Clang's tooling API.

%package -n llvm
Summary:	The Low Level Virtual Machine
Requires:	llvm%{maj_ver} = %{version}

%description -n llvm
The Low-Level Virtual Machine (LLVM) is a collection of libraries and
tools that make it easy to build compilers, optimizers, Just-In-Time
code generators, and many other compiler-related programs.

This is a dependency package providing the default llvm package.

%package -n lld
Summary:	The LLVM Linker
Requires:	lld%{maj_ver} = %{version}

%description -n lld
LLD is a new, high-performance linker. It is built as a set of reusable
components which highly leverage existing libraries in the larger LLVM
Project.

%prep
%build
%install
mkdir -p %{buildroot}/%{_bindir}
for f in %{binary_list}; do
  ln -s ../../%{install_bindir}/$f %{buildroot}%{_bindir}/$f
done

%files
%{_bindir}/clang
%{_bindir}/clang++
%{_bindir}/clang-cl
%{_bindir}/clang-cpp

%files -n clang-tools-extra 
%{_bindir}/c-index-test
%{_bindir}/clang-apply-replacements
%{_bindir}/clang-change-namespace
%{_bindir}/clang-check
%{_bindir}/clang-doc
%{_bindir}/clang-extdef-mapping
%{_bindir}/clang-format
%{_bindir}/clang-include-fixer
%{_bindir}/clang-move
%{_bindir}/clang-offload-bundler
%{_bindir}/clang-offload-packager
%{_bindir}/clang-offload-wrapper
%{_bindir}/clang-linker-wrapper
%{_bindir}/clang-nvlink-wrapper
%{_bindir}/clang-pseudo
%{_bindir}/clang-query
%{_bindir}/clang-refactor
%{_bindir}/clang-rename
%{_bindir}/clang-reorder-fields
%{_bindir}/clang-repl
%{_bindir}/clang-scan-deps
%{_bindir}/clang-tidy
%{_bindir}/clangd
%{_bindir}/diagtool
%{_bindir}/hmaptool
%{_bindir}/pp-trace
%{_bindir}/find-all-symbols
%{_bindir}/modularize
%{_bindir}/run-clang-tidy

%files -n lld
%{_bindir}/lld
%{_bindir}/lld-link
%{_bindir}/ld.lld
%{_bindir}/ld64.lld
%{_bindir}/wasm-ld

%files -n llvm
%{_bindir}/llvm-*
%{_bindir}/analyze-build
%{_bindir}/bugpoint
%{_bindir}/dsymutil
%{_bindir}/git-clang-format
%{_bindir}/intercept-build
%{_bindir}/llc
%{_bindir}/lli
%{_bindir}/opt
%{_bindir}/sancov
%{_bindir}/sanstats
%{_bindir}/scan-build
%{_bindir}/scan-build-py
%{_bindir}/scan-view
%{_bindir}/split-file
%{_bindir}/verify-uselistorder

%changelog
